import os
import platform
from command_line import command_line
from pathlib import Path
import shutil

if platform.system() != 'Windows' or os.name != 'nt':
    raise Exception('This program could only be installed on Windows NT platform!')

home = str(Path.home())
binary_directory_path = os.path.join(home, '.mujoco_matlab')

if not os.path.isdir(binary_directory_path):
    os.mkdir(binary_directory_path)

binary_name = 'mujoco_matlab.exe'

if not os.path.isfile(binary_name):
    raise Exception('Could not find application binary in the current directory!')

binary_file_path = os.path.join(binary_directory_path, binary_name)

shutil.copyfile(binary_name, binary_file_path)

(_, error, code) = command_line('SETX', 'MUJOCO_MATLAB_PATH', os.getcwd())
if code != 0:
    print(error)
    raise Exception('Failed to install the program!')

path = os.environ['PATH']
add_to_path = True

for p in path.split(';'):
    if p == binary_directory_path:
        add_to_path = False

if add_to_path:
    new_path = '{};{};'.format(path, binary_directory_path)
    (_, error, code) = command_line('SETX', 'PATH', new_path)
    if code != 0:
        print(error)
        raise Exception('Failed to install the program!')

matlab_directory_path, _ = os.path.split(shutil.which('matlab'))

if len(matlab_directory_path) < 1:
    raise Exception('Could not find MATLAB installation directory!')

matlab_engine_installer_path = os.path.join(matlab_directory_path, '..', 'extern', 'engines', 'python')

if not os.path.isdir(matlab_engine_installer_path):
    raise Exception('Could not find MATLAB-Python engine directory!')
if not os.path.isfile(os.path.join(matlab_engine_installer_path, 'setup.py')):
    raise Exception('Could not find MATLAB-Python engine installator!')

old_working_directory = os.getcwd()
os.chdir(matlab_engine_installer_path)

(out, error, code) = command_line('python', 'setup.py', 'install')
print(out, error)
if code != 0:
    raise Exception('Could not install MATLAB-Python engine!')

os.chdir(old_working_directory)

print('OK!')