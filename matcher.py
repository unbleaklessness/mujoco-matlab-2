def match(string, opening_sequence = '#{{', closing_sequence = '}}#'):
    matches = []
    inner_indexes = []
    outer_indexes = []
    opening_sequence_length = len(opening_sequence)
    closing_sequence_length = len(closing_sequence)
    string_length = len(string)
    start_indexes = []
    end_indexes = []
    inside = False
    level = 0
    if string_length > opening_sequence_length + closing_sequence_length:
        for i in range(string_length - closing_sequence_length + 1):
            if inside:
                buffer = string[i:i + closing_sequence_length]
            else:
                buffer = string[i:i + opening_sequence_length]
            if buffer == opening_sequence:
                if inside:
                    level += 1
                else:
                    start_indexes.append(i + opening_sequence_length)
                    inside = True
            if buffer == closing_sequence:
                if level == 0:
                    end_indexes.append(i)
                    inside = False
                else:
                    level -= 1
    if len(start_indexes) == len(end_indexes):
        for start, end in zip(start_indexes, end_indexes):
            matches.append(string[start:end])
            inner_indexes.append((start, end))
            outer_indexes.append((start - opening_sequence_length, end + closing_sequence_length))
    else:
        raise Exception('Unmatched opening / closing sequence.')
    return (matches, inner_indexes, outer_indexes)

def sequenced_match(string, sequences = [('#{{', '}}#')], callback = None):
    result = string
    for k, sequence in enumerate(sequences):
        new_result = ''
        matches, _, outer_indexes = match(result, opening_sequence = sequence[0], closing_sequence = sequence[1])
        n_matches = len(matches)

        if n_matches == 0:
            new_result = result
        else:
            new_result += result[0:outer_indexes[0][0]]
            for i in range(n_matches):
                if callback:
                    new_result += callback(matches[i], k)
                if i != n_matches - 1:
                    new_result += result[outer_indexes[i][1]:outer_indexes[i + 1][0]]
            new_result += result[outer_indexes[len(outer_indexes) - 1][1]:len(result)]
        result = new_result
    return result
