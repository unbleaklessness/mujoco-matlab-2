package main

import (
	"bytes"
	"fmt"
	"os"
	"os/exec"
	"path"
)

const (
	pythonMainScript      = "mujoco_matlab.py"
	pythonInterpreter     = "python"
	mujocoMatlabEVariable = "MUJOCO_MATLAB_PATH"
)

func fileExists(path string) bool {
	i, e := os.Stat(path)
	return e == nil && !i.IsDir()
}

func commandExists(command string) bool {
	_, e := exec.LookPath(command)
	return e == nil
}

func main() {
	if len(os.Args) < 4 {
		fmt.Println(`Wrong arguments. First argument must be a path to MuJoCo XML model, second argument must be a path to MuJoCo key file 'mjkey.txt', third argument must be a path to Simulink SLX model directory!`)
		os.Exit(1)
	}

	xmlModelPath := os.Args[1]
	mujocoKeyPath := os.Args[2]
	simulinkProjectPath := os.Args[3]

	rest := []string{}
	if len(os.Args) > 4 {
		rest = os.Args[4:]
	}

	wd := os.Getenv(mujocoMatlabEVariable)
	if len(wd) < 1 {
		fmt.Println("This program is not installed!")
		os.Exit(1)
	}

	if !fileExists(path.Join(wd, pythonMainScript)) {
		fmt.Println("Main Python script does not exist!")
		os.Exit(1)
	}

	if !commandExists(pythonInterpreter) {
		fmt.Println("Could not find Python interpreter!")
		os.Exit(1)
	}

	arguments := []string{
		path.Join(wd, pythonMainScript),
		xmlModelPath,
		mujocoKeyPath,
		simulinkProjectPath,
	}
	arguments = append(arguments, rest...)

	command := exec.Command(pythonInterpreter, arguments...)

	var output bytes.Buffer
	var errors bytes.Buffer

	command.Stdout = &output
	command.Stderr = &errors

	e := command.Run()

	fmt.Println(output.String())
	fmt.Println(errors.String())

	if e != nil {
		os.Exit(1)
	}

	os.Exit(0)
}
