import os
import platform
from command_line import command_line
import shutil
from pathlib import Path

if platform.system() != 'Windows' or os.name != 'nt':
    raise Exception('This program could only be uninstalled on Windows NT platform!')

home = str(Path.home())
binary_directory_path = os.path.join(home, '.mujoco_matlab')

if os.path.isdir(binary_directory_path):
    shutil.rmtree(binary_directory_path)

(_, error, code) = command_line('REG', 'DELETE', 'HKCU\\Environment', '/F', '/V', 'MUJOCO_MATLAB_PATH')
if code != 0:
    print(error)
    raise Exception('Failed to uninstall the program!')

path = os.environ['PATH']
new_path = ''

for p in path.split(';'):
    if p != binary_directory_path:
        new_path += p + ';'

(_, error, code) = command_line('SETX', 'PATH', new_path)
if code != 0:
    print(error)
    raise Exception('Failed to install the program!')

print('OK!')