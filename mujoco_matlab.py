import sys
import os
import shutil
import numbers
import platform
import os
import collections

from command_line import command_line
import matcher
import fix_indentation
import model_parser

scope_globals = {}
scope_locals = {}
engine = None

def is_matlab_array(value):
    try:
        import matlab
        if (isinstance(value, matlab.double) or isinstance(value, matlab.single) or
            isinstance(value, matlab.int8) or isinstance(value, matlab.int16) or
            isinstance(value, matlab.int32) or isinstance(value, matlab.int64) or
            isinstance(value, matlab.uint8) or isinstance(value, matlab.uint16) or
            isinstance(value, matlab.uint32) or isinstance(value, matlab.uint64)):
            return True
    except: pass
    return False

def cpp_handler(match, index):
    global scope_globals, scope_locals
    result = ''
    if index == 0:
        result = ''
    elif index == 1:
        exec(fix_indentation.fix(match), scope_globals, scope_locals)
    elif index == 2:
        result = str(eval(match, scope_globals, scope_locals))
    return result

def xml_handler(match, index):
    global scope_globals, scope_locals
    result = ''
    if index == 0:
        result = ''
    elif index == 1:
        result = ''
    elif index == 2:
        exec(fix_indentation.fix(match), scope_globals, scope_locals)
    elif index == 3:
        expression = eval(match, scope_globals, scope_locals)
        if is_matlab_array(expression) or type(expression) is list:
            for x in expression:
                result += ' '.join(map(str, x))
        else:
            result = str(expression)
    return result

def xml_matcher(data):
    return matcher.sequenced_match(data, [('-{{', '}}-'), ('<!--', '-->'), ('${{', '}}$'), ('#{{', '}}#')], xml_handler)

def cpp_matcher(data):
    return matcher.sequenced_match(data, [('-{{', '}}-'), ('${{', '}}$'), ('#{{', '}}#')], cpp_handler)

def exists_and_file(path):
    return os.path.exists(path) and os.path.isfile(path)

def get_application_directory():
    mujoco_matlab_path = os.environ['MUJOCO_MATLAB_PATH']
    if len(mujoco_matlab_path) > 0:
        os.chdir(mujoco_matlab_path)
        return mujoco_matlab_path
    else:
        print('Warning! Application is not installed, using the current directory to search for required files!')
        return os.getcwd()

def steal_matlab_workspace(name):
    global scope_globals, engine

    try:
        import matlab.engine
    except:
        print('Warning! MATLAB-Python engine is not installed, could not get workspace variables!')
        return

    available_engines = matlab.engine.find_matlab()

    if len(available_engines) < 1:
        print('Warning! No shared MATLAB session is found!')
        return

    engine = matlab.engine.connect_matlab(available_engines[0])

    try:
        for key, value in engine.workspace[name].items():
            if engine.isvector(value) and engine.isnumeric(value):
                if engine.size(value)[0][0] > 1:
                    scope_globals[key] = engine.transpose(value)
                else:
                    scope_globals[key] = value
    except:
        print('Warning! Could not find structure `{}` in the MATLAB workspace!'.format(name))
        return

def main():
    global scope_globals, scope_locals

    if len(sys.argv) < 4:
        raise Exception(
            'Wrong arguments. '
            'First argument must be a path to MuJoCo XML model, '
            'second argument must be a path to MuJoCo key file `mjkey.txt`, '
            'third argument must be a path to Simulink SLX model directory!')

    model_file_path = sys.argv[1]
    key_file_path = sys.argv[2]
    slx_directory_path = sys.argv[3]
    workspace_variable_name = ''

    if len(sys.argv) > 4:
        workspace_variable_name = sys.argv[4]

    if not os.path.isfile(model_file_path):
        raise Exception('Provided path to MuJoCo XML model does not exist or not a file!')
    if not os.path.isfile(key_file_path):
        raise Exception('Provided path to MuJoCo key file `mjkey.txt` does not exist or not a file!')
    if not os.path.isdir(slx_directory_path):
        raise Exception('Provided path to Simulink SLX model directory does not exist or not a directory!')

    model_file_path = os.path.abspath(model_file_path)
    key_file_path = os.path.abspath(key_file_path)
    slx_directory_path = os.path.abspath(slx_directory_path)

    f = open(model_file_path, 'r')
    data = f.read()
    f.close()

    if len(workspace_variable_name) > 0:
        steal_matlab_workspace(workspace_variable_name)

    output_model_file_path = os.path.join(slx_directory_path, 'MuJoCoModel.xml')
    f = open(output_model_file_path, 'w')
    f.write(xml_matcher(data))
    f.close()

    (nq, nv, nu, n_sensors, n_actuators, initial_positions, initial_velocities) = model_parser.parse(output_model_file_path)

    scope_globals['positions0'] = initial_positions
    scope_globals['velocities0'] = initial_velocities

    scope_globals['number_of_inputs'] = n_actuators
    scope_globals['number_of_outputs'] = n_sensors

    application_directory = get_application_directory()

    temporary_key_file_path = os.path.join(application_directory, 'mjkey.txt')
    shutil.copyfile(key_file_path, temporary_key_file_path)

    model_tester_file_path = os.path.join(application_directory, 'mujoco200_win', 'testxml.exe')
    (out, error, code) = command_line(model_tester_file_path, output_model_file_path)
    print(out, error)
    if code != 0:
        raise Exception('Invalid MuJoCo XML model!')

    os.remove(temporary_key_file_path)

    cpp_template_file_path = os.path.join(application_directory, 'template.cpp')
    f = open(cpp_template_file_path, 'r')
    data = f.read()
    f.close()

    output_mex_file_path = os.path.join(slx_directory_path, 'MuJoCo.mexw64')
    output_cpp_file_path = os.path.join(slx_directory_path, 'MuJoCo.cpp')
    f = open(output_cpp_file_path, 'w')
    f.write(cpp_matcher(data))
    f.close()

    if shutil.which('mex'):
        if engine:
            engine.eval("clear('MuJoCo')", nargout=0)
        (out, error, code) = command_line('mex', output_cpp_file_path, '-output', output_mex_file_path, '-g', '-Imujoco200_win', '-Lmujoco200_win', '-lmujoco200', '-lglfw3')
        print(out, error)
        if code != 0:
            raise Exception('Invalid MuJoCo XML model!')
    else:
        raise Exception('Cannot find MEX compiler!')

    mujoco_project_library_file_path = os.path.join(slx_directory_path, 'mujoco200.dll')
    mujoco_local_library_file_path = os.path.join(application_directory, 'mujoco200_win', 'mujoco200.dll')
    if not exists_and_file(mujoco_project_library_file_path):
        shutil.copyfile(mujoco_local_library_file_path, mujoco_project_library_file_path)

    glfw_project_library_file_path = os.path.join(slx_directory_path, 'glfw3.dll')
    glfw_local_library_file_path = os.path.join(application_directory, 'mujoco200_win', 'glfw3.dll')
    if not exists_and_file(glfw_project_library_file_path):
        shutil.copyfile(glfw_local_library_file_path, glfw_project_library_file_path)

    key_project_file_path = os.path.join(slx_directory_path, 'mjkey.txt')
    if not exists_and_file(key_project_file_path):
        shutil.copyfile(key_file_path, key_project_file_path)

    print('OK!')

if __name__ == '__main__':
    main()