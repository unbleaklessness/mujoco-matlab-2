import xml.etree.ElementTree as ET

def recursive_find(node, name, depth):
    result = []
    for n in node.findall(name):
        result.append((n, depth))
    for n in node:
        found, depth = recursive_find(n, name, depth + 1)
        for f in found:
            result.append(f)
    return result, depth

def recursive_find_many(node, names):
    founds = []
    for name in names:
        found, _ = recursive_find(node, name, 0)
        founds = founds + found
    result = []
    for f in sorted(founds, key=lambda x: x[1]):
        result.append(f[0])
    return result

def handle_position0(initial_positions, attributes, joint_type, n): 
    if 'position0' in attributes:
        position0 = attributes['position0']
        attributes.pop('position0')
        position0 = [float(x) for x in position0.split(' ')]
        if len(position0) == n:
            initial_positions = initial_positions + position0
        else:
            raise Exception('Initial position of joint of `{}` type must consist of {} numbers.'.format(joint_type, n))
    else:
        default_position = []
        for _ in range(n):
            default_position.append(0)
        initial_positions = initial_positions + default_position
    return initial_positions

def handle_velocity0(initial_velocities, attributes, joint_type, n): 
    if 'velocity0' in attributes:
        velocity0 = attributes['velocity0']
        attributes.pop('velocity0')
        velocity0 = [float(x) for x in velocity0.split(' ')]
        if len(velocity0) == n:
            initial_velocities = initial_velocities + velocity0
        else:
            raise Exception('Initial velocity of joint of `{}` type must consist of {} numbers.'.format(joint_type, n))
    else:
        default_velocity = []
        for _ in range(n):
            default_velocity.append(0)
        initial_velocities = initial_velocities + default_velocity
    return initial_velocities

def parse(path):
    tree = ET.parse(path)
    root = tree.getroot()

    nq = 0 # Number of generalized coordinates.
    nv = 0 # Number of degrees of freedom.
    nu = 0 # Number of actuators / controls.
    n_sensors = 0 # Number of sensor fields.
    n_actuators = 0 # Number of actuators.
    initial_positions = [] # Actuators initial positions.
    initial_velocities = [] # Actuators initial velocities.

    worldbody_node = root.find('worldbody')
    if not worldbody_node:
        raise Exception('Worldbody tag is not found.')

    joints = recursive_find_many(worldbody_node, ['joint', 'freejoint'])
    if not joints:
        raise Exception('No joints are found.')

    for joint in joints:
        attributes = joint.attrib

        if joint.tag == 'freejoint':
            nq += 7
            nv += 6
            nu += 6
            initial_positions = handle_position0(initial_positions, attributes, joint.tag, 7)
            initial_velocities = handle_velocity0(initial_velocities, attributes, joint.tag, 6)
            continue

        if not 'type' in attributes: # Hinge joint.
            nq += 1
            nv += 1
            nu += 1
            initial_positions = handle_position0(initial_positions, attributes, 'hinge', 1)
            initial_velocities = handle_velocity0(initial_velocities, attributes, 'hinge', 1)
        else:
            joint_type = attributes['type']
            if joint_type == 'ball':
                nq += 4
                nv += 3
                nu += 3
                initial_positions = handle_position0(initial_positions, attributes, joint_type, 4)
                initial_velocities = handle_velocity0(initial_velocities, attributes, joint_type, 3)
            elif joint_type == 'slide':
                nq += 1
                nv += 1
                nu += 1
                initial_positions = handle_position0(initial_positions, attributes, joint_type, 1)
                initial_velocities = handle_velocity0(initial_velocities, attributes, joint_type, 1)
            elif joint_type == 'hinge':
                nq += 1
                nv += 1
                nu += 1
                initial_positions = handle_position0(initial_positions, attributes, joint_type, 1)
                initial_velocities = handle_velocity0(initial_velocities, attributes, joint_type, 1)
            else:
                raise Exception('Unsupported joint type.')

    actuator_node = root.find('actuator')
    if not actuator_node:
        raise Exception('Actuator tag is not found.')

    all_actuators = {
        'motor': 1,
        'velocity': 1,
        'position': 1,
        'general': 1,
        'cylinder': 1,
        'muscle': 1,
    }

    actuators = recursive_find_many(actuator_node, ['motor', 'velocity', 'position'])
    if not actuators:
        raise Exception('Not actuators are found.')

    for actuator in actuators:
        for key, value in all_actuators.items():
            if actuator.tag == key:
                n_actuators += value

    sensor_node = root.find('sensor')
    if not sensor_node:
        raise Exception('Sensor tag is not found.')

    all_sensors = {
        'jointpos': 1,
        'jointvel': 1,
        'touch': 1,
        'accelerometer': 3,
        'gyro': 3,
        'velocimeter': 3,
        'force': 3,
        'torque': 3,
        'magnetometer': 3,
        'rangefinder': 1,
        'tendonpos': 1,
        'tendonvel': 1,
        'actuatorpos': 1,
        'actuatorvel': 1,
        'actuatorfrc': 1,
        'ballquat': 4,
        'ballangvel': 3,
        'jointlimitpos': 1,
        'jointlimitvel': 1,
        'jointlimitfrc': 1,
        'tendonlimitpos': 1,
        'tendonlimitvel': 1,
        'tendonlimitfrc': 1,
        'framepos': 3,
        'framequat': 4,
        'framexaxis': 3,
        'frameyaxis': 3,
        'framezaxis': 3,
        'framelinvel': 3,
        'frameangvel': 3,
        'framelinacc': 3,
        'frameangacc': 3,
        'subtreecom': 3,
        'subtreelinvel': 3,
        'subtreeangmom': 3,
        'subtreeangmom': 3,
    }

    sensors = recursive_find_many(sensor_node, all_sensors.keys())
    if not sensors:
        raise Exception('No sensors are found.')

    for sensor in sensors:
        for key, value in all_sensors.items():
            if sensor.tag == key:
                n_sensors += value

    f = open(path, 'wb')
    f.write(ET.tostring(root))
    f.close()

    return (nq, nv, nu, n_sensors, n_actuators, initial_positions, initial_velocities)
