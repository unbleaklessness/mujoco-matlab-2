import math

def fix(string):
    result = string.strip()
    new_line = '\n'
    indentation = '    '
    indentation_length = len(indentation)
    last_level = 0
    i = 0
    while True:
        if i >= len(result):
            break
        if result[i] == new_line:
            current_level = 0
            while result[i + 1:i + 1 + indentation_length] == indentation:
                current_level += 1
                level_difference = abs(last_level - current_level) - 1
                if level_difference != 0:
                    result = result[:i + 1] + result[i + 1 + indentation_length:]
                else:
                    i += indentation_length
            last_level = current_level
        i += 1
    return result