import subprocess

def command_line(*command):
    process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    return (*map(lambda x: x.decode('utf-8', errors='ignore'), process.communicate()), process.returncode)